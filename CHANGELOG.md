# Gift Card Wallet

## Changelog
### [0.1.2] - 2018-03-26
* Fixed bug which caused CODE 93 barcodes to always throw an exception.
* Cleaned failed barcode generation handling to default back to no barcode if one cannot be generated.
* Reversed numeric / text defaults such that barcode numbers are numeric by default.
* Editing is now allowed for already existing gift cards.
* Added option to auto maximise brightness when viewing gift card with a barcode
* Added EAN barcode support

### [0.1.1] - 2018-03-19
* Users can create and manage gift cards with numeric / text fields and a numeric pin.
* Users can create and manage gift card types with custom labels for gift card numbers.
* Barcodes can be generated from first number in a gift card (CODE 39, CODE 93, CODE 128, QR Code support).

## Future Release Plans
* Setting for gift card PIN numbers to be hidden by default until tapped.
* Setting when creating a gift card to not use credit (useful for cards which don't hold a monetary value such as a supermarket loyalty card).
* Improve list views to show gift card name and type in the main list, as well as value.
* Add option to delete gift card from gift card details page at the bottom (current method is to long press it in the list).
* Integrate gift card type to be below gift card name (not in the scrolling section).
* Allow current value in number spinner to be entered during a deduction / credit set without pressing enter button.
* Folder feature for storing gift cards into.
* Expiry date for a gift card.
* Sort gift cards by name, date of expiry, amount remaining, gift card type.
